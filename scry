#!/usr/bin/env python3
import scrypy as scp
import json
import sys
import argparse
import copy


preset_truncations = {
	"decklist": (
		"prices", "image_uris", "related_uris", "legalities", "purchase_uris",
		"preview", "edhrec_rank", "prints_search_uri", "rulings_uri",
		"scryfall_set_uri", "set_search_uri", "set_uri", "all_parts",
		"multiverse_ids", "tcgplayer_id", "cardmarket_id"
	),
	"just-basics": (
		"prices", "image_uris", "related_uris", "legalities", "purchase_uris",
		"preview", "edhrec_rank", "prints_search_uri", "rulings_uri",
		"scryfall_set_uri", "set_search_uri", "set_uri", "all_parts",
		"object", "oracle_id", "id", "multiverse_ids", "tcgplayer_id",
		"cardmarket_id", "lang", "released_at", "uri", "highres_image",
		"image_status", "keywords", "games", "foil", "nonfoil", "finishes",
		"oversized", "promo", "reprint", "variation", "set_id", "set_type",
		"digital", "artist_ids", "full_art", "textless", "booster", "story_spotlight",
		"illustation_id", "mtgo_id", "mtgo_foil_id"
	)
}


def make_dict(namespace):
	d = {}

	if type(namespace) in {str, bool, int, float} or namespace is None:
		return namespace

	for k, v in namespace.__dict__.items():
		if type(v) in {str, bool, int, float} or v is None:
			d[k] = v
		elif type(v) in {list, tuple, set}:
			d[k] = []
			for i in v:
				d[k].append(make_dict(i))
		else:
			d[k] = make_dict(v)
	return d


def search(query, nresults, show_links, output_file, truncate_output, use_data, add_to_file, disallow_duplicates):
	if use_data is None:
		results = scp.search(query)
		if results['object'] == "error":
			print(f"\033[31mError\033[0m: {results}")
			sys.exit(1)
		results = results['data']
	else:
		results = use_data

	output_for_file = []

	for index, card in enumerate(results):
		if nresults is not None and index not in nresults:
			continue

		print(f"\x1b[1mResult {index+1}\x1b[0m")
		print(f"\x1b[1m{card['name']}\x1b[0m" + ("" if not 'mana_cost' in card else f" ({scp.mana(card['mana_cost'])})"))
		print(f"\x1b[37m[{card['type_line']}]\x1b[0m {card['rarity'][0].upper()}")

		if "oracle_text" in card: print(f"{card['oracle_text']}")
		if "flavor_text" in card: print(f"\x1b[3m{card['flavor_text']}\x1b[0m")

		if "power" in card and "toughness" in card:
			print(f"(\x1b[1m{card['power']}\x1b[0m/\x1b[1m{card['toughness']}\x1b[0m)")
		if "loyalty" in card: print(f"[\x1b[1m{card['loyalty']}\x1b[0m]")

		print(f"🎨 {card['artist']}")

		if show_links and "scryfall_uri" in card:
			print(card['scryfall_uri'])

		print()

		if output_file is not None or add_to_file is not None:
			for t in truncate_output:
				try:  # try/except is marginally faster than `if/in` here
					del card[t]
				except KeyError: pass

			output_for_file.append(card)

	if add_to_file is not None:
		with open(add_to_file, 'r') as f:
			existing = json.load(f)

		to_write = [*existing, *output_for_file]
		if disallow_duplicates:
			existing_names = [i['name'] for i in existing]
			to_write = [*existing, *[i for i in output_for_file if i['name'] not in existing_names]]

		with open(add_to_file, 'w') as f:
			f.write(json.dumps(to_write, separators=(',', ':')))
	elif output_file is not None:
		with open(output_file, 'w+') as f:
			f.write(json.dumps(output_for_file, separators=(',', ':')))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		prog = "scry",
		description = "Search for MTG cards from the CLI!",
	)

	parser.add_argument(
		"-q", "--query",
		required = "-f" not in sys.argv,
		help = "The query to search with, see the search syntax for Scryfall for info."
	)
	parser.add_argument(
		"-f", "--use-file",
		dest = "use_file",
		help = "Display cards stored in a json file. If a query is provided with -f, the query is ignored."
	)
	parser.add_argument(
		"-n", "--nresults",
		dest = "nresults",
		help = "The results to show, if you pass 1, the first result is shown. If you pass 2,6, the second and sixth results are shown."
	)
	parser.add_argument(
		"-l", "--show-links",
		dest = "show_links",
		action = "store_true",
		help = "Toggles Scryfall links to each card."
	)
	parser.add_argument(
		"-o", "--output",
		dest = "output_file",
		help = "Writes to the provided output file with the json of all cards found."
	)
	parser.add_argument(
		"-t", "--truncate-output",
		dest = "truncate_output",
		nargs = "+",
		default = [],
		help = "When passed, the json output wrote to the output file are truncated to exclude the provided options."
	)
	parser.add_argument(
		"-T", "--truncate-preset",
		dest = "truncate_preset",
		help = "Use a preset truncation. If -t and -T are passed, the preset from -T is merged with the options passed in -t.",
		choices = list(preset_truncations.keys())
	)
	parser.add_argument(
		"-a", "--add-to-file",
		dest = "add_to_file",
		help = "Add the results to a file, pretty much just -o but it appends rather than rewrites. -o and -a cannot both be passed.",
	)
	parser.add_argument(
		"-D", "--disallow-dupes",
		dest = "disallow_dupes",
		action = "store_true",
		help = "Ignores duplicate cards found in searching when appending. I.e, json includes ['Desert'], to add includes ['Mox Lotus', 'Desert', 'Black Lotus'], the new json is ['Desert', 'Mox Lotus', 'Black Lotus']. Only works with -a.",
	)
	args = parser.parse_args()

	if args.add_to_file is not None and args.output_file is not None:
		parser.error("cannot pass -o and -a at the same time")
	elif args.disallow_dupes and args.add_to_file is None:
		parser.error("cannot pass -D without -a.")

	use_data = None
	if args.use_file is not None:
		with open(args.use_file, 'r') as f:
			use_data = json.load(f)

	if args.query is not None or args.use_file is not None:
		search(
			args.query,
			None if args.nresults is None else [int(i)-1 for i in args.nresults.split(",")],
			show_links = args.show_links,
			output_file = args.output_file,
			truncate_output = [*args.truncate_output, *(preset_truncations[args.truncate_preset] if args.truncate_preset is not None else [])],
			use_data = use_data,
			add_to_file = args.add_to_file,
			disallow_duplicates = args.disallow_dupes
		)
