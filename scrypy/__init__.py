import urllib3
import urllib.parse
import json

API = "https://api.scryfall.com"
_http = urllib3.PoolManager()

def mana(formatted_mana_cost: str) -> str:
	"""Converts Scryfall mana formatting to colored formatting for terminals.

	`{B}{R}` -> `\x1b[37mB\x1b[0m\x1b[31mR\x1b[0m`
	"""
	return (
		formatted_mana_cost
		.replace("{", "")
		.replace("}", "")
		.replace("W", "W")
		.replace("U", "\x1b[34mU\x1b[0m")
		.replace("B", "\x1b[37mB\x1b[0m")
		.replace("R", "\x1b[31mR\x1b[0m")
		.replace("G", "\x1b[33mG\x1b[0m")
	)

def _do(path: str, args: dict):
	a = "&".join([f"{k}={urllib.parse.quote(v)}" for k, v in args.items()])
	return _http.request("GET", API + path + f"?{a}")

def search(query: str) -> dict:
	"""Search with query `query` and return the results.
	`query` uses the Scryfall search syntax.
	"""
	return json.loads(_do("/cards/search", {
		"q": query
	}).data.decode("utf-8"))

