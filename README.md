# Scrypy

Minimalistic Python bindings for the Scryfall HTTP API.

Also includes a `scry` executable, which allows searching for MTG cards using the CLI!

## "Rate Limiting"

**Note**: Please respect Scryfall's request on their API to not flood them with requests. Use `time.sleep()` if needed.

As this is an API, the programmer decides how to do this, not me. Just using `time.sleep(0.1)` is a short amount of time, and when compared to the time to search for cards anyways, the end-user won't see the difference.

**Quote from Scryfall:**

> *We kindly ask that you insert 50 – 100 milliseconds of delay between the requests you send to the server at api.scryfall.com. (i.e., 10 requests per second on average).*

## API Usage

```python
import scrypy as scp

for result in scp.search("t:creature emrakul")["data"]:
	print("Found creature card containing `emrakul` in its name! Name: " + result["name"])
```

## CLI Usage

```
usage: scry [-h] -q QUERY [-f USE_FILE] [-n NRESULTS] [-l] [-o OUTPUT_FILE]
            [-t TRUNCATE_OUTPUT [TRUNCATE_OUTPUT ...]]
            [-T {decklist,just-basics}] [-a ADD_TO_FILE] [-D]

Search for MTG cards from the CLI!

optional arguments:
  -h, --help            show this help message and exit
  -q QUERY, --query QUERY
                        The query to search with, see the search syntax for
                        Scryfall for info.
  -f USE_FILE, --use-file USE_FILE
                        Display cards stored in a json file. If a query is
                        provided with -f, the query is ignored.
  -n NRESULTS, --nresults NRESULTS
                        The results to show, if you pass 1, the first result
                        is shown. If you pass 2,6, the second and sixth
                        results are shown.
  -l, --show-links      Toggles Scryfall links to each card.
  -o OUTPUT_FILE, --output OUTPUT_FILE
                        Writes to the provided output file with the json of
                        all cards found.
  -t TRUNCATE_OUTPUT [TRUNCATE_OUTPUT ...], --truncate-output TRUNCATE_OUTPUT [TRUNCATE_OUTPUT ...]
                        When passed, the json output wrote to the output file
                        are truncated to exclude the provided options.
  -T {decklist,just-basics}, --truncate-preset {decklist,just-basics}
                        Use a preset truncation. If -t and -T are passed, the
                        preset from -T is merged with the options passed in
                        -t.
  -a ADD_TO_FILE, --add-to-file ADD_TO_FILE
                        Add the results to a file, pretty much just -o but it
                        appends rather than rewrites. -o and -a cannot both be
                        passed.
  -D, --disallow-dupes  Ignores duplicate cards found in searching when
                        appending. I.e, json includes ['Desert'], to add
                        includes ['Mox Lotus', 'Desert', 'Black Lotus'], the
                        new json is ['Desert', 'Mox Lotus', 'Black Lotus'].
                        Only works with -a.
```
